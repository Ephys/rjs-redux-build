"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getStore = getStore;
exports.registerReducer = registerReducer;
exports.registerSaga = registerSaga;
exports.registerStorePart = registerStorePart;
Object.defineProperty(exports, "storePart", {
  enumerable: true,
  get: function () {
    return _decorators.storePart;
  }
});
Object.defineProperty(exports, "StorePart", {
  enumerable: true,
  get: function () {
    return _decorators.storePart;
  }
});
Object.defineProperty(exports, "saga", {
  enumerable: true,
  get: function () {
    return _decorators.saga;
  }
});
Object.defineProperty(exports, "Saga", {
  enumerable: true,
  get: function () {
    return _decorators.saga;
  }
});
Object.defineProperty(exports, "reducer", {
  enumerable: true,
  get: function () {
    return _decorators.reducer;
  }
});
Object.defineProperty(exports, "Reducer", {
  enumerable: true,
  get: function () {
    return _decorators.reducer;
  }
});
Object.defineProperty(exports, "action", {
  enumerable: true,
  get: function () {
    return _decorators.action;
  }
});
Object.defineProperty(exports, "Action", {
  enumerable: true,
  get: function () {
    return _decorators.action;
  }
});
Object.defineProperty(exports, "throttleSaga", {
  enumerable: true,
  get: function () {
    return _decorators.throttleSaga;
  }
});
Object.defineProperty(exports, "withRedux", {
  enumerable: true,
  get: function () {
    return _withRedux.default;
  }
});
Object.defineProperty(exports, "WithRedux", {
  enumerable: true,
  get: function () {
    return _withRedux.default;
  }
});

var _ClientSideHook = require("./hooks/ClientSideHook");

var _installReducers = require("./install-reducers");

var _getCurrentStore = _interopRequireDefault(require("./get-current-store"));

var _installSagas = require("./install-sagas");

var _decorators = require("./storePart/decorators");

var _withRedux = _interopRequireDefault(require("./withRedux"));

function getStore() {
  const store = (0, _getCurrentStore.default)();

  if (process.env.NODE_ENV !== 'production') {
    if (!store) {
      throw new Error('[redux] Cannot get store. Either this is a bug or getStore has been called outside of a react context.');
    }
  }

  return store;
}

function registerReducer(reducer) {
  (0, _installReducers.installReducers)(getStore(), reducer);
} // export function registerGlobalReducer(reducer) {
//   if (allStores.size > 0) {
//     throw new Error('Global reducers cannot be registered after a store has been created due to SSR.');
//   }
//
//   addGlobalReducer(reducer);
// }
// export function registerGlobalStorePart(storePart) {
//   registerGlobalReducer(storePart[Symbols.reducer]);
//   registerGlobalSaga(storePart[Symbols.sagas]);
// }
// ==


function registerSaga(saga) {
  (0, _installSagas.installSagas)(getStore(), saga);
} // export function registerGlobalSaga(sagas) {
//   if (!Array.isArray(sagas)) {
//     sagas = [sagas];
//   }
//
//   for (const saga of sagas) {
//     addGlobalSaga(saga);
//   }
//
//   for (const store of allStores) {
//     installSagas(store);
//   }
// }


function registerStorePart(storePart) {
  registerReducer(storePart[_decorators.Symbols.reducer]);
  registerSaga(storePart[_decorators.Symbols.sagas]);
} // TODO:
// client: use singleton store
// server: hook on all active stores? (as these functions are very likely to be run only once).
// or on current thread-local store (but must be loaded during component loading. Maybe using a decorator?)
// @usesReducer(xxx)
// @usesSaga(xxx)
// OR BOTH!
// @withActions()
// @withStore()
// @withProvider()
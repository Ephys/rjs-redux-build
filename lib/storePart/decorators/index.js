"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.isProvider = isProvider;
exports.throttleSaga = throttleSaga;
Object.defineProperty(exports, "storePart", {
  enumerable: true,
  get: function () {
    return _StorePartDecorator.default;
  }
});
Object.defineProperty(exports, "reducer", {
  enumerable: true,
  get: function () {
    return _ReducerDecorator.default;
  }
});
Object.defineProperty(exports, "saga", {
  enumerable: true,
  get: function () {
    return _SagaDecorator.default;
  }
});
Object.defineProperty(exports, "action", {
  enumerable: true,
  get: function () {
    return _ActionDecorator.default;
  }
});
exports.Symbols = void 0;

var _effects = require("redux-saga/effects");

var _StorePartDecorator = _interopRequireDefault(require("./provider/StorePartDecorator"));

var _ReducerDecorator = _interopRequireDefault(require("./provider/ReducerDecorator"));

var _SagaDecorator = _interopRequireDefault(require("./provider/SagaDecorator"));

var _ActionDecorator = _interopRequireDefault(require("./provider/ActionDecorator"));

const Symbols = {
  sagas: Symbol('storePart-sagas'),
  reducer: Symbol('storePart-reducer'),
  name: Symbol('name')
};
exports.Symbols = Symbols;

function isProvider(item) {
  if (typeof item !== 'function') {
    return false;
  }

  return Object.prototype.hasOwnProperty.call(item, Symbols.reducer);
}

function throttleSaga(ms) {
  return function throttleBridge(pattern, saga, ...args) {
    return (0, _effects.throttle)(ms, pattern, saga, ...args);
  };
}
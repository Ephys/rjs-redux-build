"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.ACTION_TYPE_DYNAMIC = void 0;

var _reselect = require("reselect");

var _constantCase = _interopRequireDefault(require("constant-case"));

var _effects = require("redux-saga/effects");

var _logger = _interopRequireDefault(require("@reworkjs/core/logger"));

var _utils = require("../../../utils/utils");

var _index = require("../index");

var _methodUtil = require("../../../utils/method-util");

var _decorators = require("../../../utils/decorators");

var _ReducerDecorator = require("./ReducerDecorator");

var _SagaDecorator = require("./SagaDecorator");

var _ActionDecorator = require("./ActionDecorator");

var _util = require("./_util");

const ACTION_TYPE_DYNAMIC = Symbol('action-type-dynamic');
exports.ACTION_TYPE_DYNAMIC = ACTION_TYPE_DYNAMIC;
const PROVIDER_APP_STATE_ACCESSOR = Symbol('app-state-holder');
const PROVIDER_STATE_ACCESSOR = Symbol('state-holder');
const mutatedProperties = Symbol('mutatedProperties'); // Blacklist universal properties, Function static properties and @storePart symbols.

const PROPERTY_BLACKLIST = Object.getOwnPropertyNames(Object.prototype) // use noop instead of Function because Function does not have the "caller" nor "arguments" properties on
// Safari < 11
// eslint-disable-next-line
.concat(Object.getOwnPropertyNames(function noop() {})).concat('displayName'); // used by chrome, same purpose as 'name'.

function denyAccess() {
  throw new Error('Cannot access @storePart state outside of @reducer annotated methods. If you are trying to r/w the state from a @saga, you will need to use "yield put(this.<reducerMethodName>())"');
}

const IMMUTABLE_STATE = {
  set: denyAccess,
  get: denyAccess
};
const registeredDomains = [];

function parseOptions(options) {
  if (options.length > 1) {
    throw new TypeError('@storePart accepts only one argument. Try @storePart({ domain: <string> }) or @storePart(<string>).');
  }

  if (!options[0]) {
    return {};
  }

  switch (typeof options[0]) {
    case 'string':
    case 'symbol':
      return {
        domain: options[0]
      };

    case 'object':
      {
        const domain = options[0].domain;

        if (typeof domain !== 'string' && typeof domain !== 'symbol') {
          throw new TypeError(`@storePart({ domain: ${JSON.stringify(domain)} }): Invalid domain. Expected string.`);
        }

        return options[0];
      }

    default:
      throw new TypeError(`@storePart(${JSON.stringify(options[0])}): Invalid first argument. Expected string or object.`);
  }
}
/**
 * Decorator that transforms a static class into a provider
 *
 * how it works:
 * - Methods become action builders,
 *   calling them will create an action of the right type with the parameters as payload.
 * - The logic of those methods is moved inside reducers and sagas
 *   depending on if they are annotated with @reducer or @saga
 * - Getting fields return a selector
 * - Setting fields throw an error if called from outside
 *   a @reducer annotated method and call state.set(fieldName, fieldValue) otherwise
 */


const StorePartDecorator = (0, _decorators.classDecorator)(arg => {
  // if (arg.options.length > 0) {
  //   throw new TypeError('@storePart does not accept options.');
  // }
  const providerClass = arg.target;

  if (Object.getOwnPropertyNames(providerClass.prototype).length > 1) {
    _logger.default.warn(`@storePart ${providerClass.name} has instance properties. This is likely a bug as providers are fully static.`);
  }

  const options = parseOptions(arg.options);

  if (!options.domain) {
    _logger.default.warn(`No domain specified for Provider ${providerClass.name}. Using the class name instead. This might be an issue with minified code as domains names might collide.`);
  }

  const domainIdentifier = options.domain || `RJS-${providerClass.name}`;

  if (registeredDomains.includes(domainIdentifier)) {
    throw new Error(`A provider has already been registered with the domain ${JSON.stringify(domainIdentifier)}. Please make sure all provider domains are unique.`);
  }

  registeredDomains.push(domainIdentifier);

  function selectDomain() {
    return state => {
      const domain = state[domainIdentifier];

      if (domain === void 0) {
        throw new Error(`Could not retrieve the store subdomain ${JSON.stringify(domainIdentifier)}. Either the associated reducer is not loaded or it is loaded under a different name.`);
      }

      return domain;
    };
  }

  providerClass[PROVIDER_STATE_ACCESSOR] = IMMUTABLE_STATE;
  const {
    initialState,
    sagaList,
    actionListeners
  } = extractFromProvider(providerClass, selectDomain, domainIdentifier);

  function genericReducer(state = initialState, actionData) {
    const type = actionData.type;
    const actionHandler = actionListeners.get(type);

    if (!actionHandler) {
      return state;
    }

    const mutatedObjectMap = new Map();
    providerClass[mutatedProperties] = mutatedObjectMap;
    providerClass[PROVIDER_STATE_ACCESSOR] = state;

    for (const actionHandlerItem of actionHandler) {
      if (Array.isArray(actionData.payload)) {
        actionHandlerItem.apply(providerClass, actionData.payload);
      } else {
        actionHandlerItem.call(providerClass, actionData.payload);
      }
    }

    providerClass[PROVIDER_STATE_ACCESSOR] = IMMUTABLE_STATE;
    providerClass[mutatedProperties] = IMMUTABLE_STATE;

    if (Object.keys(mutatedObjectMap).size === 0) {
      return state;
    }

    return { ...state,
      ...(0, _utils.mapToObject)(mutatedObjectMap)
    };
  }

  genericReducer[_index.Symbols.name] = domainIdentifier;
  (0, _methodUtil.attemptChangeName)(genericReducer, `${providerClass.name}@reducer`);
  providerClass[_index.Symbols.reducer] = genericReducer;
  providerClass[_index.Symbols.sagas] = sagaList;
  return providerClass;
});

StorePartDecorator.select = function select(property, store) {
  if (!store) {
    throw new TypeError('Expected store (not state) as second parameter.');
  }

  if (typeof property !== 'function' || !property.recomputations) {
    throw new TypeError('The property you passed to Provider.select is not selectable.');
  }

  return property(store.getState());
};

var _default = StorePartDecorator;
exports.default = _default;

function extractFromProvider(ProviderClass, selectDomain, domainIdentifier) {
  const actionListeners = new Map();
  const sagaList = [];
  const initialState = {};
  const dataBag = {
    ProviderClass,
    sagaList,
    actionListeners,
    initialState,
    selectDomain,
    domainIdentifier,

    addActionListener(actionType, actionListener) {
      if (!actionListeners.has(actionType)) {
        actionListeners.set(actionType, []);
      }

      actionListeners.get(actionType).push(actionListener);
    }

  };
  const keys = Object.getOwnPropertyNames(ProviderClass);

  for (const propertyName of keys) {
    if (PROPERTY_BLACKLIST.includes(propertyName)) {
      continue;
    }

    const property = ProviderClass[propertyName];
    const type = property == null ? null : property[_util.propertyType];

    switch (type) {
      case _ActionDecorator.TYPE_ACTION_GENERATOR:
        continue;

      case _ReducerDecorator.TYPE_REDUCER:
        extractReducer(propertyName, dataBag);
        continue;

      case _SagaDecorator.TYPE_SAGA:
        extractSaga(propertyName, dataBag);
        continue;

      case _util.TYPE_STATE:
      default:
        extractState(propertyName, dataBag);
    }
  }

  dataBag.initialState = initialState;
  return dataBag;
}

function installActionBuilder(dataBag, propertyName) {
  const providerClass = dataBag.ProviderClass;
  const method = providerClass[propertyName];
  const metadata = (0, _util.getPropertyMetadata)(method);

  if (!metadata.actionType) {
    return (0, _methodUtil.killMethod)(providerClass, propertyName);
  }

  const actionType = parseActionType(metadata.actionType, dataBag.domainIdentifier, propertyName);

  function createAction(...args) {
    return {
      type: actionType,
      payload: args
    };
  }

  createAction.actionType = actionType;
  (0, _methodUtil.replaceMethod)(providerClass, propertyName, createAction);
  return createAction;
}

function parseActionType(actionType, domainIdentifier, propertyName) {
  if (actionType === ACTION_TYPE_DYNAMIC) {
    return `@@${(0, _constantCase.default)(domainIdentifier)}/${(0, _constantCase.default)(propertyName)}`;
  }

  return actionType;
}

function extractReducer(propertyName, dataBag) {
  const {
    ProviderClass,
    addActionListener
  } = dataBag;
  const actionListener = ProviderClass[propertyName];
  const metadata = (0, _util.getPropertyMetadata)(actionListener);

  for (const actionType of metadata.listenedActionTypes) {
    addActionListener(parseActionType(actionType, dataBag.domainIdentifier, propertyName), actionListener);
  }

  installActionBuilder(dataBag, propertyName);
}

function extractSaga(propertyName, dataBag) {
  const {
    ProviderClass,
    sagaList
  } = dataBag;
  const property = ProviderClass[propertyName];
  const metadata = (0, _util.getPropertyMetadata)(property);
  let callActionHandler;
  const actionBuilder = installActionBuilder(dataBag, propertyName);

  if (metadata.trackStatus) {
    const {
      initialState,
      addActionListener
    } = dataBag;
    const trackActionType = `@@${(0, _constantCase.default)(dataBag.domainIdentifier)}/SET_RUNNING/${(0, _constantCase.default)(propertyName)}`;

    callActionHandler = function* callActionHandlerWithTracking(action) {
      try {
        yield (0, _effects.put)({
          type: trackActionType,
          payload: true
        });
        yield* property.apply(ProviderClass, action.payload);
      } finally {
        yield (0, _effects.put)({
          type: trackActionType,
          payload: false
        });
      }
    };

    const runningPropertyName = `${propertyName}.running`;
    installSelector(runningPropertyName, dataBag);
    initialState[runningPropertyName] = false;
    Object.defineProperty(actionBuilder, 'running', Object.getOwnPropertyDescriptor(ProviderClass, runningPropertyName));
    addActionListener(trackActionType, function changeRunningStatus(runningStatus) {
      this[runningPropertyName] = runningStatus; // eslint-disable-line
    });
  } else {
    callActionHandler = function* callActionHandlerWithoutTracking(action) {
      yield* property.apply(ProviderClass, action.payload);
    };
  }

  if (metadata.listenedActionTypes && metadata.listenedActionTypes.size > 0) {
    const actionWatchers = [];
    const takeFunctions = metadata.takeFunctions;
    const tfKeys = takeFunctions ? Object.keys(takeFunctions).map(Number).sort((a, b) => a - b) : null;
    let i = 0;

    for (const listenedActionType of metadata.listenedActionTypes) {
      // select the takeFunction that was used in that @saga decorator.
      const takeFunction = tfKeys ? takeFunctions[orderedClampUp(tfKeys, i)] : _effects.takeLatest;
      const realActionType = parseActionType(listenedActionType, dataBag.domainIdentifier, propertyName);
      actionWatchers.push(takeFunction(realActionType, callActionHandler));
      i++;
    }

    function* awaitAction() {
      // eslint-disable-line
      yield (0, _effects.all)(actionWatchers);
    }

    (0, _methodUtil.attemptChangeName)(awaitAction, `${ProviderClass.name}.${property.name}`);
    sagaList.push(awaitAction);
  }
}

function orderedClampUp(numbers, i) {
  for (const number of numbers) {
    if (number > i) {
      return number;
    }
  }

  return numbers[numbers.length - 1];
}

function extractState(propertyName, dataBag) {
  const {
    ProviderClass,
    initialState
  } = dataBag;
  const descriptor = Object.getOwnPropertyDescriptor(ProviderClass, propertyName);

  if (!descriptor) {
    throw new TypeError('Tried getting the descriptor of something that does not exist.');
  }

  if (!descriptor.configurable) {
    throw new TypeError(`@storePart could not redefine property ${JSON.stringify(propertyName)} because it is non-configurable.`);
  }

  if (descriptor.set) {
    throw new TypeError('setters are currently incompatible with @storePart.');
  }

  if (descriptor.get) {
    installGetterSelector(propertyName, dataBag);
  } else {
    initialState[propertyName] = ProviderClass[propertyName];
    installSelector(propertyName, dataBag);
  }
}

function installGetterSelector(propertyName, dataBag) {
  const {
    ProviderClass
  } = dataBag;
  const originalGetter = Object.getOwnPropertyDescriptor(ProviderClass, propertyName).get;

  function getterSelector(applicationState) {
    const originalStateAccessor = ProviderClass[PROVIDER_APP_STATE_ACCESSOR];
    ProviderClass[PROVIDER_APP_STATE_ACCESSOR] = applicationState;
    const result = originalGetter.call(ProviderClass); // eslint-disable-line
    // restore original one in case we're being called by another getter. We don't want to give null back to them.

    ProviderClass[PROVIDER_APP_STATE_ACCESSOR] = originalStateAccessor;
    return result;
  }

  Object.defineProperty(dataBag.ProviderClass, propertyName, {
    get() {
      // Called by a reducer, state properties already return the actual value.
      if (ProviderClass[PROVIDER_STATE_ACCESSOR] !== IMMUTABLE_STATE) {
        return originalGetter.call(ProviderClass);
      } // Called by a state getter. Call the selector with the app state to return the actual value.


      if (ProviderClass[PROVIDER_APP_STATE_ACCESSOR]) {
        return getterSelector(ProviderClass[PROVIDER_APP_STATE_ACCESSOR]);
      } // Called from outside. Return a selector.


      return getterSelector;
    }

  });
}

function installSelector(propertyName, dataBag) {
  const {
    ProviderClass,
    selectDomain
  } = dataBag;
  const selectProperty = (0, _reselect.createSelector)(selectDomain(), subState => subState[propertyName]);
  (0, _methodUtil.attemptChangeName)(selectProperty, `select_${propertyName}`); // create selector

  Object.defineProperty(ProviderClass, propertyName, {
    get() {
      // Called by a state getter. Call the selector with the app state to return the actual value.
      if (ProviderClass[PROVIDER_APP_STATE_ACCESSOR]) {
        return selectProperty(ProviderClass[PROVIDER_APP_STATE_ACCESSOR]);
      } // called from outside. Return selector.


      const providerState = ProviderClass[PROVIDER_STATE_ACCESSOR];

      if (providerState === IMMUTABLE_STATE) {
        return selectProperty;
      } // Called from reducer, return actual value.


      if (ProviderClass[mutatedProperties].has(propertyName)) {
        return ProviderClass[mutatedProperties].get(propertyName);
      }

      return providerState[propertyName];
    },

    set(value) {
      ProviderClass[mutatedProperties].set(propertyName, value);
    }

  });
}
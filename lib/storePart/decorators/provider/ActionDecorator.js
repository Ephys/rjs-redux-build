"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.TYPE_ACTION_GENERATOR = void 0;

var _decorators = require("../../../utils/decorators");

var _util = require("./_util");

const TYPE_ACTION_GENERATOR = Symbol('TYPE_ACTION_GENERATOR');
exports.TYPE_ACTION_GENERATOR = TYPE_ACTION_GENERATOR;
const USAGE = '@action';

var _default = (0, _decorators.methodDecorator)(arg => {
  const {
    descriptor,
    options,
    target
  } = arg;
  const boundProperty = descriptor.value.bind(target);

  if (options.length > 0) {
    throw new TypeError(`${USAGE} does not accept any arguments.`);
  }

  if (!(0, _util.setPropertyType)(boundProperty, TYPE_ACTION_GENERATOR)) {
    throw new TypeError(`${USAGE}: Cannot be used on a method that has already been marked as either a @saga or a @reducer.`);
  }

  descriptor.value = boundProperty;
  return descriptor;
});

exports.default = _default;
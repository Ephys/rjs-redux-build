"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPropertyMetadata = getPropertyMetadata;
exports.setPropertyType = setPropertyType;
exports.TYPE_STATE = exports.propertyMetadata = exports.propertyType = void 0;
const propertyType = Symbol('property-type');
exports.propertyType = propertyType;
const propertyMetadata = Symbol('is-reducer');
exports.propertyMetadata = propertyMetadata;
const TYPE_STATE = Symbol('TYPE_STATE');
exports.TYPE_STATE = TYPE_STATE;

function getPropertyMetadata(property) {
  const reducerData = property[propertyMetadata] || {};
  property[propertyMetadata] = reducerData;
  return reducerData;
}

function setPropertyType(property, type) {
  if (property[propertyType] && property[propertyType] !== type) {
    return false;
  }

  property[propertyType] = type;
  return true;
}
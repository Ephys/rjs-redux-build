"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.transform = transform;
exports.default = exports.TYPE_SAGA = void 0;

var _effects = require("redux-saga/effects");

var _decorators = require("../../../utils/decorators");

var _util = require("./_util");

var _ReducerDecorator = require("./ReducerDecorator");

const TYPE_SAGA = Symbol('TYPE_SAGA');
exports.TYPE_SAGA = TYPE_SAGA;
const USAGE = '@saga([actionType || { actionType, trackStatus }])';

var _default = (0, _decorators.methodDecorator)(arg => {
  parseOptions(arg);

  if (!(0, _util.setPropertyType)(arg.descriptor.value, TYPE_SAGA)) {
    throw new TypeError(`${USAGE}: Cannot be used on a method that has already been marked as either a @reducer or an @action.`);
  }

  return transform(arg);
});

exports.default = _default;

function parseOptions(arg) {
  const options = (0, _ReducerDecorator.parseOptions)(arg);

  if (options.trackStatus != null && typeof options.trackStatus !== 'boolean') {
    throw new TypeError(`${USAGE}: Invalid option trackStatus: expected boolean.`);
  }
}

function transform(arg) {
  const descriptor = (0, _ReducerDecorator.transform)(arg);
  const metadata = (0, _util.getPropertyMetadata)(arg.descriptor.value);

  if (arg.options.trackStatus) {
    metadata.trackStatus = true;
  }

  if (arg.options.take) {
    const take = arg.options.take;
    let takeFunction = null;

    if (typeof take === 'string') {
      switch (take) {
        case 'latest':
          takeFunction = _effects.takeLatest;
          break;

        case 'every':
          takeFunction = _effects.takeEvery;
          break;

        default:
      }
    } else if (typeof take === 'function') {
      takeFunction = take;
    }

    if (!takeFunction) {
      throw new TypeError(`Invalid value for take (@saga({ take: ${JSON.stringify(take)} }))`);
    }

    if (!metadata.takeFunctions) {
      metadata.takeFunctions = {};
    }

    metadata.takeFunctions[metadata.listenedActionTypes.size] = takeFunction;
  }

  return descriptor;
}
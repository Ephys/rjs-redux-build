"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "default", {
  enumerable: true,
  get: function () {
    return _generateGlobalStoreParts.default;
  }
});

var _generateGlobalStoreParts = _interopRequireDefault(require("val-loader!./_generate-global-store-parts"));
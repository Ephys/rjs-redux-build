"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _async_hooks = _interopRequireDefault(require("async_hooks"));

class ThreadLocalValue {
  constructor() {
    this._values = new Map();

    const asyncHook = _async_hooks.default.createHook({
      init: (asyncId, type, triggerAsyncId) => {
        const existing = this._values.get(triggerAsyncId);

        if (existing) {
          this._values.set(asyncId, existing);
        }
      },
      destroy: asyncId => {
        this._values.delete(asyncId);
      }
    });

    asyncHook.enable();
  }

  set(val) {
    this._values.set(_async_hooks.default.executionAsyncId(), val);
  }

  get() {
    return this._values.get(_async_hooks.default.executionAsyncId());
  }

}

exports.default = ThreadLocalValue;
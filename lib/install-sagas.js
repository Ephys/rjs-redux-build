"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.installSagas = installSagas;

var _utils = require("./utils/utils");

function installSagas(store, sagas = []) {
  sagas = (0, _utils.toArray)(sagas);

  for (const saga of sagas) {
    store.runSaga(saga);
  }
}
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.methodDecorator = methodDecorator;
exports.classDecorator = classDecorator;
exports.isDecoratedClass = isDecoratedClass;
exports.isDecoratedMethod = isDecoratedMethod;

function methodDecorator(logicHandler) {
  return function methodDecoratorWrapper(...decorateeOrOptions) {
    if (isDecoratedMethod(decorateeOrOptions)) {
      return logicHandler({
        target: decorateeOrOptions[0],
        key: decorateeOrOptions[1],
        descriptor: decorateeOrOptions[2],
        options: []
      });
    }

    const options = decorateeOrOptions;
    return function methodDecoratorWithOptionsWrapper(...decoratee) {
      if (!isDecoratedMethod(decoratee)) {
        throw new TypeError(`Rejected decorator syntax "@${logicHandler.name}()()", only one parenthesis set is allowed at most.`);
      }

      return logicHandler({
        target: decoratee[0],
        key: decoratee[1],
        descriptor: decoratee[2],
        options
      });
    };
  };
}

function classDecorator(logicHandler) {
  return function classDecoratorWrapper(...decorateeOrOptions) {
    if (isDecoratedClass(decorateeOrOptions)) {
      return logicHandler({
        target: decorateeOrOptions[0],
        options: []
      });
    }

    const options = decorateeOrOptions;
    return function methodDecoratorWithOptionsWrapper(...decoratee) {
      if (!isDecoratedClass(decoratee)) {
        throw new TypeError(`Rejected decorator syntax "@${logicHandler.name}()()", only one parenthesis set is allowed at most.`);
      }

      return logicHandler({
        target: decoratee[0],
        options
      });
    };
  };
}

function isDecoratedClass(args) {
  return Array.isArray(args) && args[0] instanceof Function;
}

function isDecoratedMethod(args) {
  return Array.isArray(args) && args[0] instanceof Function && typeof args[1] === 'string' && typeof args[2] === 'object' && args[2] !== null;
}
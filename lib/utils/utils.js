"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.toArray = toArray;
exports.mapToObject = mapToObject;

function toArray(obj) {
  if (obj == null) {
    return [];
  }

  if (Array.isArray(obj)) {
    return obj;
  }

  return [obj];
}

function mapToObject(map) {
  const obj = Object.create(null);

  for (const [key, value] of map.entries()) {
    obj[key] = value;
  }

  return obj;
}
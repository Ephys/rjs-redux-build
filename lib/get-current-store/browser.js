"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getCurrentStore;

var _ClientSideHook = require("../hooks/ClientSideHook");

/**
 * Returns the store of the currently rendering react app.
 *
 * On the client side, the store is created only once and is a singleton.
 */
function getCurrentStore() {
  // allStores should only ever have one item on client-side.
  return _ClientSideHook.allStores.values().next().value;
}
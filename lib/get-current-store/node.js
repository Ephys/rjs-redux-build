"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getCurrentStore;

var _ServerSideHook = require("../hooks/ServerSideHook");

/**
 * Returns the store of the currently rendering react app.
 *
 * On the server side, the store is depends on the
 * current execution context.
 *
 * This ensures we get the right store, even
 * if the react instance that is currently being rendered
 * changes due to async operations.
 */
function getCurrentStore() {
  return _ServerSideHook.currentStoreTl.get();
}
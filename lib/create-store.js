"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = createConfiguredStore;

var _redux = require("redux");

var _reduxSaga = _interopRequireDefault(require("redux-saga"));

var _global = _interopRequireDefault(require("global"));

var _installSagas = require("./install-sagas");

var _installReducers = require("./install-reducers");

var _decorators = require("./storePart/decorators");

var _globalStoreParts = _interopRequireDefault(require("./storePart/global-store-parts"));

// TODO reinstall debug (only front-end)
// import debug from '@reworkjs/core/debug';
// debug.store
const devtools = _global.default.devToolsExtension || (() => noop => noop);

function createConfiguredStore() {
  // Create the store with two middlewares
  // 1. sagaMiddleware: Makes redux-sagas work
  // 2. routerMiddleware: Syncs the location/URL path to the state
  const sagaMiddleware = (0, _reduxSaga.default)();
  const middlewares = [sagaMiddleware // TODO re-install react-router-redux? Is it useful?
  // routerMiddleware(history),
  ];

  if (process.env.NODE_ENV !== 'production') {
    // ensure state is immutable (check is disabled in production).
    middlewares.unshift(require('redux-immutable-state-invariant').default());
  }

  const enhancers = [replaceActionDispatcher, (0, _redux.applyMiddleware)(...middlewares), devtools()];
  const initialState = _global.default.__REDUX_SSR__ || {};
  const initialReducers = [];
  const initialSagas = [];

  for (const storeModule of (0, _globalStoreParts.default)()) {
    const store = storeModule.default || storeModule;
    initialReducers.push(store[_decorators.Symbols.reducer]);
    initialSagas.push(...store[_decorators.Symbols.sagas]);
  }
  /* this part is too tightly coupled with install-reducers */


  const initialReducersMap = (0, _installReducers.reducerArrayToMap)(initialReducers);
  const store = (0, _redux.createStore)((0, _redux.combineReducers)(initialReducersMap), initialState, (0, _redux.compose)(...enhancers));
  (0, _installReducers.markReducerInstalled)(store, initialReducersMap);
  /* end of coupled part */

  const activeSagas = []; // Create hook for async sagas

  store.runSaga = function runSaga(saga) {
    if (activeSagas.includes(saga)) {
      return;
    }

    sagaMiddleware.run(saga);
    activeSagas.push(saga);
  };

  (0, _installSagas.installSagas)(store, initialSagas);
  return store;
}

function replaceActionDispatcher(actualCreateStore) {
  return function fakeCreateStore(a, b, c) {
    const store = actualCreateStore(a, b, c);
    const nativeDispatch = store.dispatch;
    Object.assign(store, {
      dispatch(arg) {
        if (Array.isArray(arg)) {
          for (const action of arg) {
            nativeDispatch.call(this, action);
          }
        } else {
          nativeDispatch.call(this, arg);
        }
      }

    });
    return store;
  };
}
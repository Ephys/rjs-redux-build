"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _path = _interopRequireDefault(require("path"));

var _package = _interopRequireDefault(require("../package"));

class ReduxPlugin {
  constructor(params) {
    const config = params.pluginConfig;

    const configDir = _path.default.dirname(params.configFile);

    this.globalStoresDir = config['global-stores'] ? _path.default.resolve(configDir, config['global-stores']) : null;
  }

  getHooks() {
    return {
      client: _path.default.resolve(`${__dirname}/../hook-client`),
      server: _path.default.resolve(`${__dirname}/../hook-server`)
    };
  }

  getInstallableDependencies() {
    return _package.default.peerDependencies || {};
  }

}

exports.default = ReduxPlugin;
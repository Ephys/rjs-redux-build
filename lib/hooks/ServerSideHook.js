"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.currentStoreTl = void 0;

var _ThreadLocalValue = _interopRequireDefault(require("../ThreadLocalValue"));

var _ClientSideHook = _interopRequireDefault(require("./ClientSideHook"));

const currentStoreTl = new _ThreadLocalValue.default();
exports.currentStoreTl = currentStoreTl;

class ServerSideHook extends _ClientSideHook.default {
  constructor() {
    super();
    currentStoreTl.set(this.store);
  }

  preRender(generatedHtmlParts) {
    generatedHtmlParts.footer += `<script>window.__REDUX_SSR__ = ${JSON.stringify(this.store.getState())}</script>`;
  }

}

exports.default = ServerSideHook;
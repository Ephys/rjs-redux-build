"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.allStores = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRedux = require("react-redux");

var _createStore = _interopRequireDefault(require("../create-store"));

var _jsxFileName = "/Users/ephys/Documents/dev/reworkjs/redux/src/hooks/ClientSideHook.js";
const allStores = new Set();
exports.allStores = allStores;

class ClientSideHook {
  constructor() {
    this.store = (0, _createStore.default)(); // only used by client app

    allStores.add(this.store);
  }

  wrapRootComponent(component) {
    return _react.default.createElement(_reactRedux.Provider, {
      store: this.store,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 21
      },
      __self: this
    }, component);
  }

  postRequest() {
    allStores.delete(this.store);
  }

}

exports.default = ClientSideHook;
# cp -r ../react-framework/lib/ lib
# cp -r ../react-framework/package.json package.json
cd ../redux
npm run build
cd ../redux-build

rm -rf lib src
rsync -ar --progress ../redux/ ./ --exclude 'node_modules' --exclude '.git' --exclude '.gitignore'
rm -rf .git
git init
git add --all
git commit -n -m "build"

git checkout -B develop
git remote add origin git@bitbucket.org:Ephys/rjs-redux-build.git
git push --force origin master

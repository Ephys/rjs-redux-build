import _classCallCheck from "@babel/runtime/helpers/esm/classCallCheck";
import _createClass from "@babel/runtime/helpers/esm/createClass";
import asyncHooks from 'async_hooks';

var ThreadLocalValue =
/*#__PURE__*/
function () {
  function ThreadLocalValue() {
    var _this = this;

    _classCallCheck(this, ThreadLocalValue);

    this._values = new Map();
    var asyncHook = asyncHooks.createHook({
      init: function init(asyncId, type, triggerAsyncId) {
        var existing = _this._values.get(triggerAsyncId);

        if (existing) {
          _this._values.set(asyncId, existing);
        }
      },
      destroy: function destroy(asyncId) {
        _this._values["delete"](asyncId);
      }
    });
    asyncHook.enable();
  }

  _createClass(ThreadLocalValue, [{
    key: "set",
    value: function set(val) {
      this._values.set(asyncHooks.executionAsyncId(), val);
    }
  }, {
    key: "get",
    value: function get() {
      return this._values.get(asyncHooks.executionAsyncId());
    }
  }]);

  return ThreadLocalValue;
}();

export { ThreadLocalValue as default };
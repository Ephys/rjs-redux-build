import { combineReducers } from 'redux';
import { toArray } from './utils/utils';
import { Symbols } from './storePart/decorators/index';
var REGISTERED_REDUCERS_KEY = Symbol('registered-reducers');
/**
 * Creates a reducer that uses exclusively
 */

export function reducerArrayToMap(reducers) {
  var reducerMap = Object.create(null);
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = reducers[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var reducer = _step.value;
      reducerMap[getReducerName(reducer)] = reducer;
    }
  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator["return"] != null) {
        _iterator["return"]();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  Object.freeze(reducerMap);
  return reducerMap;
}
export function markReducerInstalled(store, reducers) {
  store[REGISTERED_REDUCERS_KEY] = reducers;
}

function getReducerName(reducer) {
  return reducer[Symbols.name] || reducer.name;
}
/**
 *
 * @param store
 * @param reducers
 */


export function installReducers(store) {
  var reducers = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
  reducers = toArray(reducers);
  var registeredReducers = store[REGISTERED_REDUCERS_KEY] ? Object.assign(Object.create(null), store[REGISTERED_REDUCERS_KEY]) : Object.create(null);
  var dirty = false;
  var _iteratorNormalCompletion2 = true;
  var _didIteratorError2 = false;
  var _iteratorError2 = undefined;

  try {
    for (var _iterator2 = reducers[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
      var reducer = _step2.value;
      var reducerName = getReducerName(reducer);

      if (process.env.NODE_ENV !== 'production') {
        if (typeof reducerName !== 'string' || !reducerName) {
          throw new TypeError('injectAsyncReducer: reducer is missing a name.');
        }

        if (typeof reducer !== 'function') {
          throw new TypeError('injectAsyncReducer: reducer is not a function.');
        }
      }

      var existingReducer = registeredReducers[reducerName];

      if (existingReducer) {
        if (existingReducer !== reducers) {
          continue;
        }

        throw new Error("Trying to register two different reducers sharing the same name \"".concat(reducerName, "\"."));
      }

      registeredReducers[reducerName] = reducer;
      dirty = true;
    }
  } catch (err) {
    _didIteratorError2 = true;
    _iteratorError2 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
        _iterator2["return"]();
      }
    } finally {
      if (_didIteratorError2) {
        throw _iteratorError2;
      }
    }
  }

  if (dirty) {
    Object.freeze(registeredReducers);
    markReducerInstalled(store, registeredReducers);
    store.replaceReducer(combineReducers(registeredReducers));
  }
} // TODO: StoreParts should register their reducer one way or another.

/*
  for (const provider: Provider of providers) {

    const reducer = provider[Symbols.reducer] || provider.reducer;
    if (!reducer) {
      continue;
    }

    if (process.env.NODE_ENV !== 'production') {
      if (typeof reducer !== 'function') {
        throw new Error('One of your providers exported a reducer that is not a function');
      }
    }

    const name = reducer[Symbols.name] || reducer.name;

    if (process.env.NODE_ENV !== 'production') {
      if (!name) {
        throw new Error('One of your providers exported a nameless reducer, please name it');
      }
    }

    reducers[name] = reducer;
  }
 */

/*
TODO: replace reducer on all stores if hot module
// Make reducers hot reloadable, see http://mxs.is/googmo
if (module.hot) {
  const createReducers = require('../create-reducer').default;
  const nextReducers = createReducers(store.asyncReducers);

  store.replaceReducer(nextReducers);
}
*/
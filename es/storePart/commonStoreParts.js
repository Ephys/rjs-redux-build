var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/**
 * Exports the list of providers based on the setup config.
 *
 * Using webpack.
 */

import { getModulesFromWpContext } from '../utils/webpack-utils';
import { isProvider } from './decorators/index';

var providers = getModulesFromWpContext(require.context('@@directories.providers', true, /\.jsx?$/)).filter(selectProvider);

function selectProvider(item) {
  if (item == null) {
    return false;
  }

  if (isProvider(item)) {
    return true;
  }

  if ((typeof item === 'undefined' ? 'undefined' : _typeof(item)) === 'object') {
    return Object.prototype.hasOwnProperty.call(item, 'reducer') || Object.prototype.hasOwnProperty.call(item, 'sagas');
  }

  return false;
}

export default providers;
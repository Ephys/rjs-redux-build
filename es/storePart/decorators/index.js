import { throttle as originalThrottle } from 'redux-saga/effects';
export { default as storePart } from './provider/StorePartDecorator';
export { default as reducer } from './provider/ReducerDecorator';
export { default as saga } from './provider/SagaDecorator';
export { default as action } from './provider/ActionDecorator';
export var Symbols = {
  sagas: Symbol('storePart-sagas'),
  reducer: Symbol('storePart-reducer'),
  name: Symbol('name')
};
export function isProvider(item) {
  if (typeof item !== 'function') {
    return false;
  }

  return Object.prototype.hasOwnProperty.call(item, Symbols.reducer);
}
export function throttleSaga(ms) {
  return function throttleBridge(pattern, saga) {
    for (var _len = arguments.length, args = new Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
      args[_key - 2] = arguments[_key];
    }

    return originalThrottle.apply(void 0, [ms, pattern, saga].concat(args));
  };
}
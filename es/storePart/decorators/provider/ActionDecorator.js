import { methodDecorator } from '../../../utils/decorators';
import { setPropertyType } from './_util';
export var TYPE_ACTION_GENERATOR = Symbol('TYPE_ACTION_GENERATOR');
var USAGE = '@action';
export default methodDecorator(function (arg) {
  var descriptor = arg.descriptor,
      options = arg.options,
      target = arg.target;
  var boundProperty = descriptor.value.bind(target);

  if (options.length > 0) {
    throw new TypeError("".concat(USAGE, " does not accept any arguments."));
  }

  if (!setPropertyType(boundProperty, TYPE_ACTION_GENERATOR)) {
    throw new TypeError("".concat(USAGE, ": Cannot be used on a method that has already been marked as either a @saga or a @reducer."));
  }

  descriptor.value = boundProperty;
  return descriptor;
});
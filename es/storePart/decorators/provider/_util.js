export var propertyType = Symbol('property-type');
export var propertyMetadata = Symbol('is-reducer');
export var TYPE_STATE = Symbol('TYPE_STATE');
export function getPropertyMetadata(property) {
  var reducerData = property[propertyMetadata] || {};
  property[propertyMetadata] = reducerData;
  return reducerData;
}
export function setPropertyType(property, type) {
  if (property[propertyType] && property[propertyType] !== type) {
    return false;
  }

  property[propertyType] = type;
  return true;
}
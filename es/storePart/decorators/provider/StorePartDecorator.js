import _regeneratorRuntime from "@babel/runtime/regenerator";
import _defineProperty from "@babel/runtime/helpers/esm/defineProperty";
import _typeof from "@babel/runtime/helpers/esm/typeof";

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

import { createSelector } from 'reselect';
import constantCase from 'constant-case';
import { put, takeLatest, all } from 'redux-saga/effects';
import logger from '@reworkjs/core/logger';
import { mapToObject } from '../../../utils/utils';
import { Symbols } from '../index';
import { attemptChangeName, killMethod, replaceMethod } from '../../../utils/method-util';
import { classDecorator } from '../../../utils/decorators';
import { TYPE_REDUCER } from './ReducerDecorator';
import { TYPE_SAGA } from './SagaDecorator';
import { TYPE_ACTION_GENERATOR } from './ActionDecorator';
import { propertyType, TYPE_STATE, getPropertyMetadata } from './_util';
export var ACTION_TYPE_DYNAMIC = Symbol('action-type-dynamic');
var PROVIDER_APP_STATE_ACCESSOR = Symbol('app-state-holder');
var PROVIDER_STATE_ACCESSOR = Symbol('state-holder');
var mutatedProperties = Symbol('mutatedProperties'); // Blacklist universal properties, Function static properties and @storePart symbols.

var PROPERTY_BLACKLIST = Object.getOwnPropertyNames(Object.prototype) // use noop instead of Function because Function does not have the "caller" nor "arguments" properties on
// Safari < 11
// eslint-disable-next-line
.concat(Object.getOwnPropertyNames(function noop() {})).concat('displayName'); // used by chrome, same purpose as 'name'.

function denyAccess() {
  throw new Error('Cannot access @storePart state outside of @reducer annotated methods. If you are trying to r/w the state from a @saga, you will need to use "yield put(this.<reducerMethodName>())"');
}

var IMMUTABLE_STATE = {
  set: denyAccess,
  get: denyAccess
};
var registeredDomains = [];

function parseOptions(options) {
  if (options.length > 1) {
    throw new TypeError('@storePart accepts only one argument. Try @storePart({ domain: <string> }) or @storePart(<string>).');
  }

  if (!options[0]) {
    return {};
  }

  switch (_typeof(options[0])) {
    case 'string':
    case 'symbol':
      return {
        domain: options[0]
      };

    case 'object':
      {
        var domain = options[0].domain;

        if (typeof domain !== 'string' && _typeof(domain) !== 'symbol') {
          throw new TypeError("@storePart({ domain: ".concat(JSON.stringify(domain), " }): Invalid domain. Expected string."));
        }

        return options[0];
      }

    default:
      throw new TypeError("@storePart(".concat(JSON.stringify(options[0]), "): Invalid first argument. Expected string or object."));
  }
}
/**
 * Decorator that transforms a static class into a provider
 *
 * how it works:
 * - Methods become action builders,
 *   calling them will create an action of the right type with the parameters as payload.
 * - The logic of those methods is moved inside reducers and sagas
 *   depending on if they are annotated with @reducer or @saga
 * - Getting fields return a selector
 * - Setting fields throw an error if called from outside
 *   a @reducer annotated method and call state.set(fieldName, fieldValue) otherwise
 */


var StorePartDecorator = classDecorator(function (arg) {
  // if (arg.options.length > 0) {
  //   throw new TypeError('@storePart does not accept options.');
  // }
  var providerClass = arg.target;

  if (Object.getOwnPropertyNames(providerClass.prototype).length > 1) {
    logger.warn("@storePart ".concat(providerClass.name, " has instance properties. This is likely a bug as providers are fully static."));
  }

  var options = parseOptions(arg.options);

  if (!options.domain) {
    logger.warn("No domain specified for Provider ".concat(providerClass.name, ". Using the class name instead. This might be an issue with minified code as domains names might collide."));
  }

  var domainIdentifier = options.domain || "RJS-".concat(providerClass.name);

  if (registeredDomains.includes(domainIdentifier)) {
    throw new Error("A provider has already been registered with the domain ".concat(JSON.stringify(domainIdentifier), ". Please make sure all provider domains are unique."));
  }

  registeredDomains.push(domainIdentifier);

  function selectDomain() {
    return function (state) {
      var domain = state[domainIdentifier];

      if (domain === void 0) {
        throw new Error("Could not retrieve the store subdomain ".concat(JSON.stringify(domainIdentifier), ". Either the associated reducer is not loaded or it is loaded under a different name."));
      }

      return domain;
    };
  }

  providerClass[PROVIDER_STATE_ACCESSOR] = IMMUTABLE_STATE;

  var _extractFromProvider = extractFromProvider(providerClass, selectDomain, domainIdentifier),
      initialState = _extractFromProvider.initialState,
      sagaList = _extractFromProvider.sagaList,
      actionListeners = _extractFromProvider.actionListeners;

  function genericReducer() {
    var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
    var actionData = arguments.length > 1 ? arguments[1] : undefined;
    var type = actionData.type;
    var actionHandler = actionListeners.get(type);

    if (!actionHandler) {
      return state;
    }

    var mutatedObjectMap = new Map();
    providerClass[mutatedProperties] = mutatedObjectMap;
    providerClass[PROVIDER_STATE_ACCESSOR] = state;
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = actionHandler[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var actionHandlerItem = _step.value;

        if (Array.isArray(actionData.payload)) {
          actionHandlerItem.apply(providerClass, actionData.payload);
        } else {
          actionHandlerItem.call(providerClass, actionData.payload);
        }
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator["return"] != null) {
          _iterator["return"]();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }

    providerClass[PROVIDER_STATE_ACCESSOR] = IMMUTABLE_STATE;
    providerClass[mutatedProperties] = IMMUTABLE_STATE;

    if (Object.keys(mutatedObjectMap).size === 0) {
      return state;
    }

    return _objectSpread({}, state, {}, mapToObject(mutatedObjectMap));
  }

  genericReducer[Symbols.name] = domainIdentifier;
  attemptChangeName(genericReducer, "".concat(providerClass.name, "@reducer"));
  providerClass[Symbols.reducer] = genericReducer;
  providerClass[Symbols.sagas] = sagaList;
  return providerClass;
});

StorePartDecorator.select = function select(property, store) {
  if (!store) {
    throw new TypeError('Expected store (not state) as second parameter.');
  }

  if (typeof property !== 'function' || !property.recomputations) {
    throw new TypeError('The property you passed to Provider.select is not selectable.');
  }

  return property(store.getState());
};

export default StorePartDecorator;

function extractFromProvider(ProviderClass, selectDomain, domainIdentifier) {
  var actionListeners = new Map();
  var sagaList = [];
  var initialState = {};
  var dataBag = {
    ProviderClass: ProviderClass,
    sagaList: sagaList,
    actionListeners: actionListeners,
    initialState: initialState,
    selectDomain: selectDomain,
    domainIdentifier: domainIdentifier,
    addActionListener: function addActionListener(actionType, actionListener) {
      if (!actionListeners.has(actionType)) {
        actionListeners.set(actionType, []);
      }

      actionListeners.get(actionType).push(actionListener);
    }
  };
  var keys = Object.getOwnPropertyNames(ProviderClass);
  var _iteratorNormalCompletion2 = true;
  var _didIteratorError2 = false;
  var _iteratorError2 = undefined;

  try {
    for (var _iterator2 = keys[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
      var propertyName = _step2.value;

      if (PROPERTY_BLACKLIST.includes(propertyName)) {
        continue;
      }

      var property = ProviderClass[propertyName];
      var type = property == null ? null : property[propertyType];

      switch (type) {
        case TYPE_ACTION_GENERATOR:
          continue;

        case TYPE_REDUCER:
          extractReducer(propertyName, dataBag);
          continue;

        case TYPE_SAGA:
          extractSaga(propertyName, dataBag);
          continue;

        case TYPE_STATE:
        default:
          extractState(propertyName, dataBag);
      }
    }
  } catch (err) {
    _didIteratorError2 = true;
    _iteratorError2 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
        _iterator2["return"]();
      }
    } finally {
      if (_didIteratorError2) {
        throw _iteratorError2;
      }
    }
  }

  dataBag.initialState = initialState;
  return dataBag;
}

function installActionBuilder(dataBag, propertyName) {
  var providerClass = dataBag.ProviderClass;
  var method = providerClass[propertyName];
  var metadata = getPropertyMetadata(method);

  if (!metadata.actionType) {
    return killMethod(providerClass, propertyName);
  }

  var actionType = parseActionType(metadata.actionType, dataBag.domainIdentifier, propertyName);

  function createAction() {
    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return {
      type: actionType,
      payload: args
    };
  }

  createAction.actionType = actionType;
  replaceMethod(providerClass, propertyName, createAction);
  return createAction;
}

function parseActionType(actionType, domainIdentifier, propertyName) {
  if (actionType === ACTION_TYPE_DYNAMIC) {
    return "@@".concat(constantCase(domainIdentifier), "/").concat(constantCase(propertyName));
  }

  return actionType;
}

function extractReducer(propertyName, dataBag) {
  var ProviderClass = dataBag.ProviderClass,
      addActionListener = dataBag.addActionListener;
  var actionListener = ProviderClass[propertyName];
  var metadata = getPropertyMetadata(actionListener);
  var _iteratorNormalCompletion3 = true;
  var _didIteratorError3 = false;
  var _iteratorError3 = undefined;

  try {
    for (var _iterator3 = metadata.listenedActionTypes[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
      var _actionType = _step3.value;
      addActionListener(parseActionType(_actionType, dataBag.domainIdentifier, propertyName), actionListener);
    }
  } catch (err) {
    _didIteratorError3 = true;
    _iteratorError3 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion3 && _iterator3["return"] != null) {
        _iterator3["return"]();
      }
    } finally {
      if (_didIteratorError3) {
        throw _iteratorError3;
      }
    }
  }

  installActionBuilder(dataBag, propertyName);
}

function extractSaga(propertyName, dataBag) {
  var ProviderClass = dataBag.ProviderClass,
      sagaList = dataBag.sagaList;
  var property = ProviderClass[propertyName];
  var metadata = getPropertyMetadata(property);
  var callActionHandler;
  var actionBuilder = installActionBuilder(dataBag, propertyName);

  if (metadata.trackStatus) {
    var initialState = dataBag.initialState,
        addActionListener = dataBag.addActionListener;
    var trackActionType = "@@".concat(constantCase(dataBag.domainIdentifier), "/SET_RUNNING/").concat(constantCase(propertyName));
    callActionHandler =
    /*#__PURE__*/
    _regeneratorRuntime.mark(function callActionHandlerWithTracking(action) {
      return _regeneratorRuntime.wrap(function callActionHandlerWithTracking$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.prev = 0;
              _context.next = 3;
              return put({
                type: trackActionType,
                payload: true
              });

            case 3:
              return _context.delegateYield(property.apply(ProviderClass, action.payload), "t0", 4);

            case 4:
              _context.prev = 4;
              _context.next = 7;
              return put({
                type: trackActionType,
                payload: false
              });

            case 7:
              return _context.finish(4);

            case 8:
            case "end":
              return _context.stop();
          }
        }
      }, callActionHandlerWithTracking, null, [[0,, 4, 8]]);
    });
    var runningPropertyName = "".concat(propertyName, ".running");
    installSelector(runningPropertyName, dataBag);
    initialState[runningPropertyName] = false;
    Object.defineProperty(actionBuilder, 'running', Object.getOwnPropertyDescriptor(ProviderClass, runningPropertyName));
    addActionListener(trackActionType, function changeRunningStatus(runningStatus) {
      this[runningPropertyName] = runningStatus; // eslint-disable-line
    });
  } else {
    callActionHandler =
    /*#__PURE__*/
    _regeneratorRuntime.mark(function callActionHandlerWithoutTracking(action) {
      return _regeneratorRuntime.wrap(function callActionHandlerWithoutTracking$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              return _context2.delegateYield(property.apply(ProviderClass, action.payload), "t0", 1);

            case 1:
            case "end":
              return _context2.stop();
          }
        }
      }, callActionHandlerWithoutTracking);
    });
  }

  if (metadata.listenedActionTypes && metadata.listenedActionTypes.size > 0) {
    var awaitAction =
    /*#__PURE__*/
    _regeneratorRuntime.mark(function awaitAction() {
      return _regeneratorRuntime.wrap(function awaitAction$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              _context3.next = 2;
              return all(actionWatchers);

            case 2:
            case "end":
              return _context3.stop();
          }
        }
      }, awaitAction);
    });

    var actionWatchers = [];
    var takeFunctions = metadata.takeFunctions;
    var tfKeys = takeFunctions ? Object.keys(takeFunctions).map(Number).sort(function (a, b) {
      return a - b;
    }) : null;
    var i = 0;
    var _iteratorNormalCompletion4 = true;
    var _didIteratorError4 = false;
    var _iteratorError4 = undefined;

    try {
      for (var _iterator4 = metadata.listenedActionTypes[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
        var listenedActionType = _step4.value;
        // select the takeFunction that was used in that @saga decorator.
        var takeFunction = tfKeys ? takeFunctions[orderedClampUp(tfKeys, i)] : takeLatest;
        var realActionType = parseActionType(listenedActionType, dataBag.domainIdentifier, propertyName);
        actionWatchers.push(takeFunction(realActionType, callActionHandler));
        i++;
      }
    } catch (err) {
      _didIteratorError4 = true;
      _iteratorError4 = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion4 && _iterator4["return"] != null) {
          _iterator4["return"]();
        }
      } finally {
        if (_didIteratorError4) {
          throw _iteratorError4;
        }
      }
    }

    attemptChangeName(awaitAction, "".concat(ProviderClass.name, ".").concat(property.name));
    sagaList.push(awaitAction);
  }
}

function orderedClampUp(numbers, i) {
  var _iteratorNormalCompletion5 = true;
  var _didIteratorError5 = false;
  var _iteratorError5 = undefined;

  try {
    for (var _iterator5 = numbers[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
      var number = _step5.value;

      if (number > i) {
        return number;
      }
    }
  } catch (err) {
    _didIteratorError5 = true;
    _iteratorError5 = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion5 && _iterator5["return"] != null) {
        _iterator5["return"]();
      }
    } finally {
      if (_didIteratorError5) {
        throw _iteratorError5;
      }
    }
  }

  return numbers[numbers.length - 1];
}

function extractState(propertyName, dataBag) {
  var ProviderClass = dataBag.ProviderClass,
      initialState = dataBag.initialState;
  var descriptor = Object.getOwnPropertyDescriptor(ProviderClass, propertyName);

  if (!descriptor) {
    throw new TypeError('Tried getting the descriptor of something that does not exist.');
  }

  if (!descriptor.configurable) {
    throw new TypeError("@storePart could not redefine property ".concat(JSON.stringify(propertyName), " because it is non-configurable."));
  }

  if (descriptor.set) {
    throw new TypeError('setters are currently incompatible with @storePart.');
  }

  if (descriptor.get) {
    installGetterSelector(propertyName, dataBag);
  } else {
    initialState[propertyName] = ProviderClass[propertyName];
    installSelector(propertyName, dataBag);
  }
}

function installGetterSelector(propertyName, dataBag) {
  var ProviderClass = dataBag.ProviderClass;
  var originalGetter = Object.getOwnPropertyDescriptor(ProviderClass, propertyName).get;

  function getterSelector(applicationState) {
    var originalStateAccessor = ProviderClass[PROVIDER_APP_STATE_ACCESSOR];
    ProviderClass[PROVIDER_APP_STATE_ACCESSOR] = applicationState;
    var result = originalGetter.call(ProviderClass); // eslint-disable-line
    // restore original one in case we're being called by another getter. We don't want to give null back to them.

    ProviderClass[PROVIDER_APP_STATE_ACCESSOR] = originalStateAccessor;
    return result;
  }

  Object.defineProperty(dataBag.ProviderClass, propertyName, {
    get: function get() {
      // Called by a reducer, state properties already return the actual value.
      if (ProviderClass[PROVIDER_STATE_ACCESSOR] !== IMMUTABLE_STATE) {
        return originalGetter.call(ProviderClass);
      } // Called by a state getter. Call the selector with the app state to return the actual value.


      if (ProviderClass[PROVIDER_APP_STATE_ACCESSOR]) {
        return getterSelector(ProviderClass[PROVIDER_APP_STATE_ACCESSOR]);
      } // Called from outside. Return a selector.


      return getterSelector;
    }
  });
}

function installSelector(propertyName, dataBag) {
  var ProviderClass = dataBag.ProviderClass,
      selectDomain = dataBag.selectDomain;
  var selectProperty = createSelector(selectDomain(), function (subState) {
    return subState[propertyName];
  });
  attemptChangeName(selectProperty, "select_".concat(propertyName)); // create selector

  Object.defineProperty(ProviderClass, propertyName, {
    get: function get() {
      // Called by a state getter. Call the selector with the app state to return the actual value.
      if (ProviderClass[PROVIDER_APP_STATE_ACCESSOR]) {
        return selectProperty(ProviderClass[PROVIDER_APP_STATE_ACCESSOR]);
      } // called from outside. Return selector.


      var providerState = ProviderClass[PROVIDER_STATE_ACCESSOR];

      if (providerState === IMMUTABLE_STATE) {
        return selectProperty;
      } // Called from reducer, return actual value.


      if (ProviderClass[mutatedProperties].has(propertyName)) {
        return ProviderClass[mutatedProperties].get(propertyName);
      }

      return providerState[propertyName];
    },
    set: function set(value) {
      ProviderClass[mutatedProperties].set(propertyName, value);
    }
  });
}
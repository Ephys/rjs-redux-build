import _typeof from "@babel/runtime/helpers/esm/typeof";
import { methodDecorator } from '../../../utils/decorators';
import { getPropertyMetadata, setPropertyType } from './_util';
import { ACTION_TYPE_DYNAMIC } from './StorePartDecorator';
export var TYPE_REDUCER = Symbol('TYPE_REDUCER');
var EMPTY_OBJ = {};
Object.freeze(EMPTY_OBJ);
var USAGE = '@reducer([actionType])';
export default methodDecorator(function (arg) {
  parseOptions(arg);

  if (!setPropertyType(arg.descriptor.value, TYPE_REDUCER)) {
    throw new TypeError("".concat(USAGE, ": Cannot be used on a method that has already been marked as either a @saga or an @action."));
  }

  return transform(arg);
});
export function parseOptions(arg) {
  var options = arg.options;

  if (options.length > 1) {
    throw new TypeError("".concat(USAGE, " only accepts one argument. ").concat(options.length, " provided"));
  }

  if (options[0] == null) {
    return EMPTY_OBJ;
  }

  var objArg = _typeof(options[0]) === 'object' ? options[0] : {
    actionType: options[0]
  };

  if (objArg.actionType != null && typeof objArg.actionType !== 'string' && typeof objArg.actionType !== 'function') {
    throw new TypeError("".concat(USAGE, ": Invalid option actionType: expected string, a reducer/saga method, or undefined."));
  }

  arg.options = objArg;
  return objArg;
}
export function transform(arg) {
  var descriptor = arg.descriptor,
      options = arg.options;
  var property = descriptor.value;
  var metadata = getPropertyMetadata(property);
  metadata.listenedActionTypes = metadata.listenedActionTypes || new Set();

  if (!options.actionType) {
    metadata.actionType = ACTION_TYPE_DYNAMIC;
    metadata.listenedActionTypes.add(metadata.actionType);
  } else if (Array.isArray(options.actionType)) {
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = options.actionType[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var actionType = _step.value;
        metadata.listenedActionTypes.add(parseActionType(actionType));
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator["return"] != null) {
          _iterator["return"]();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  } else {
    metadata.listenedActionTypes.add(parseActionType(options.actionType));
  }

  return descriptor;
}

function parseActionType(actionType) {
  if (typeof actionType === 'function') {
    var actionHandler = actionType;

    if (!actionHandler.actionType) {
      throw new TypeError("Method ".concat(actionHandler.name, " does not have an action type. Is it correctly decorated ?"));
    }

    return actionHandler.actionType;
  }

  return actionType;
}
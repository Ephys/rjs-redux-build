/**
 * Exports the list of providers based on the setup config.
 *
 * Using webpack.
 */
// This file is run by webpack, and the code it generates is the one that will actually be used
// See: val-loader

/* eslint-disable import/no-commonjs */
var fs = require('fs');

var path = require('path');

module.exports = function getGlobalStores() {
  var getPluginInstance = require('@reworkjs/core/lib/internals/get-plugins').getPluginInstance;

  var plugin = getPluginInstance(require('../../../lib/plugin')["default"]);
  var globalStoresDir = plugin.globalStoresDir;
  var filePromise;

  if (globalStoresDir == null) {
    filePromise = Promise.resolve([]);
  } else {
    filePromise = fs.promises.readdir(globalStoresDir)["catch"](function () {
      return [];
    }).then(function (files) {
      return files.map(function (file) {
        return path.join(globalStoresDir, file);
      });
    });
  }

  return filePromise.then(function (files) {
    var importArray = "[".concat(files.map(function (hookFile) {
      return "require(".concat(JSON.stringify(hookFile), ")");
    }).join(','), "]");
    return {
      code: "export default () => ".concat(importArray, ";")
    };
  });
}; // import { getModulesFromWpContext } from '../utils/webpack-utils';
// import { isProvider } from './decorators/index';
//
// export type Provider = {
//   reducer: ?Function,
//   sagas: ?Function[],
// };
//
// const providers: Provider[] = getModulesFromWpContext(
//   require.context('@@directories.providers', true, /\.jsx?$/),
// ).filter(selectProvider);
//
// function selectProvider(item) {
//   if (item == null) {
//     return false;
//   }
//
//   if (isProvider(item)) {
//     return true;
//   }
//
//   if (typeof item === 'object') {
//     return Object.prototype.hasOwnProperty.call(item, 'reducer') || Object.prototype.hasOwnProperty.call(item, 'sagas');
//   }
//
//   return false;
// }
//
// export default providers;
//
import _classCallCheck from "@babel/runtime/helpers/esm/classCallCheck";
import _createClass from "@babel/runtime/helpers/esm/createClass";
import path from 'path';
import pkg from '../package';

var ReduxPlugin =
/*#__PURE__*/
function () {
  function ReduxPlugin(params) {
    _classCallCheck(this, ReduxPlugin);

    var config = params.pluginConfig;
    var configDir = path.dirname(params.configFile);
    this.globalStoresDir = config['global-stores'] ? path.resolve(configDir, config['global-stores']) : null;
  }

  _createClass(ReduxPlugin, [{
    key: "getHooks",
    value: function getHooks() {
      return {
        client: path.resolve("".concat(__dirname, "/../hook-client")),
        server: path.resolve("".concat(__dirname, "/../hook-server"))
      };
    }
  }, {
    key: "getInstallableDependencies",
    value: function getInstallableDependencies() {
      return pkg.peerDependencies || {};
    }
  }]);

  return ReduxPlugin;
}();

export { ReduxPlugin as default };
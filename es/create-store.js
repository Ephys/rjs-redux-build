import _toConsumableArray from "@babel/runtime/helpers/esm/toConsumableArray";
import { createStore as createReduxStore, applyMiddleware, compose, combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import global from 'global';
import { installSagas } from './install-sagas';
import { markReducerInstalled, reducerArrayToMap } from './install-reducers';
import { Symbols } from './storePart/decorators';
import loadGlobalStores from './storePart/global-store-parts'; // TODO reinstall debug (only front-end)
// import debug from '@reworkjs/core/debug';
// debug.store

var devtools = global.devToolsExtension || function () {
  return function (noop) {
    return noop;
  };
};

export default function createConfiguredStore() {
  // Create the store with two middlewares
  // 1. sagaMiddleware: Makes redux-sagas work
  // 2. routerMiddleware: Syncs the location/URL path to the state
  var sagaMiddleware = createSagaMiddleware();
  var middlewares = [sagaMiddleware // TODO re-install react-router-redux? Is it useful?
  // routerMiddleware(history),
  ];

  if (process.env.NODE_ENV !== 'production') {
    // ensure state is immutable (check is disabled in production).
    middlewares.unshift(require('redux-immutable-state-invariant')["default"]());
  }

  var enhancers = [replaceActionDispatcher, applyMiddleware.apply(void 0, middlewares), devtools()];
  var initialState = global.__REDUX_SSR__ || {};
  var initialReducers = [];
  var initialSagas = [];
  var _iteratorNormalCompletion = true;
  var _didIteratorError = false;
  var _iteratorError = undefined;

  try {
    for (var _iterator = loadGlobalStores()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
      var storeModule = _step.value;

      var _store = storeModule["default"] || storeModule;

      initialReducers.push(_store[Symbols.reducer]);
      initialSagas.push.apply(initialSagas, _toConsumableArray(_store[Symbols.sagas]));
    }
    /* this part is too tightly coupled with install-reducers */

  } catch (err) {
    _didIteratorError = true;
    _iteratorError = err;
  } finally {
    try {
      if (!_iteratorNormalCompletion && _iterator["return"] != null) {
        _iterator["return"]();
      }
    } finally {
      if (_didIteratorError) {
        throw _iteratorError;
      }
    }
  }

  var initialReducersMap = reducerArrayToMap(initialReducers);
  var store = createReduxStore(combineReducers(initialReducersMap), initialState, compose.apply(void 0, enhancers));
  markReducerInstalled(store, initialReducersMap);
  /* end of coupled part */

  var activeSagas = []; // Create hook for async sagas

  store.runSaga = function runSaga(saga) {
    if (activeSagas.includes(saga)) {
      return;
    }

    sagaMiddleware.run(saga);
    activeSagas.push(saga);
  };

  installSagas(store, initialSagas);
  return store;
}

function replaceActionDispatcher(actualCreateStore) {
  return function fakeCreateStore(a, b, c) {
    var store = actualCreateStore(a, b, c);
    var nativeDispatch = store.dispatch;
    Object.assign(store, {
      dispatch: function dispatch(arg) {
        if (Array.isArray(arg)) {
          var _iteratorNormalCompletion2 = true;
          var _didIteratorError2 = false;
          var _iteratorError2 = undefined;

          try {
            for (var _iterator2 = arg[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              var action = _step2.value;
              nativeDispatch.call(this, action);
            }
          } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
                _iterator2["return"]();
              }
            } finally {
              if (_didIteratorError2) {
                throw _iteratorError2;
              }
            }
          }
        } else {
          nativeDispatch.call(this, arg);
        }
      }
    });
    return store;
  };
}
export function getModulesFromWpContext() {
  var modules = [];

  for (var _len = arguments.length, contexts = new Array(_len), _key = 0; _key < _len; _key++) {
    contexts[_key] = arguments[_key];
  }

  for (var _i = 0, _contexts = contexts; _i < _contexts.length; _i++) {
    var context = _contexts[_i];
    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
      for (var _iterator = context.keys()[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
        var key = _step.value;
        var provider = getDefault(context(key));
        modules.push(provider);
      }
    } catch (err) {
      _didIteratorError = true;
      _iteratorError = err;
    } finally {
      try {
        if (!_iteratorNormalCompletion && _iterator["return"] != null) {
          _iterator["return"]();
        }
      } finally {
        if (_didIteratorError) {
          throw _iteratorError;
        }
      }
    }
  }

  return modules;
} // TODO: merge with rework core

function getDefault(module) {
  return module["default"] || module;
}
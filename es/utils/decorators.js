import _typeof from "@babel/runtime/helpers/esm/typeof";
export function methodDecorator(logicHandler) {
  return function methodDecoratorWrapper() {
    for (var _len = arguments.length, decorateeOrOptions = new Array(_len), _key = 0; _key < _len; _key++) {
      decorateeOrOptions[_key] = arguments[_key];
    }

    if (isDecoratedMethod(decorateeOrOptions)) {
      return logicHandler({
        target: decorateeOrOptions[0],
        key: decorateeOrOptions[1],
        descriptor: decorateeOrOptions[2],
        options: []
      });
    }

    var options = decorateeOrOptions;
    return function methodDecoratorWithOptionsWrapper() {
      for (var _len2 = arguments.length, decoratee = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        decoratee[_key2] = arguments[_key2];
      }

      if (!isDecoratedMethod(decoratee)) {
        throw new TypeError("Rejected decorator syntax \"@".concat(logicHandler.name, "()()\", only one parenthesis set is allowed at most."));
      }

      return logicHandler({
        target: decoratee[0],
        key: decoratee[1],
        descriptor: decoratee[2],
        options: options
      });
    };
  };
}
export function classDecorator(logicHandler) {
  return function classDecoratorWrapper() {
    for (var _len3 = arguments.length, decorateeOrOptions = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
      decorateeOrOptions[_key3] = arguments[_key3];
    }

    if (isDecoratedClass(decorateeOrOptions)) {
      return logicHandler({
        target: decorateeOrOptions[0],
        options: []
      });
    }

    var options = decorateeOrOptions;
    return function methodDecoratorWithOptionsWrapper() {
      for (var _len4 = arguments.length, decoratee = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
        decoratee[_key4] = arguments[_key4];
      }

      if (!isDecoratedClass(decoratee)) {
        throw new TypeError("Rejected decorator syntax \"@".concat(logicHandler.name, "()()\", only one parenthesis set is allowed at most."));
      }

      return logicHandler({
        target: decoratee[0],
        options: options
      });
    };
  };
}
export function isDecoratedClass(args) {
  return Array.isArray(args) && args[0] instanceof Function;
}
export function isDecoratedMethod(args) {
  return Array.isArray(args) && args[0] instanceof Function && typeof args[1] === 'string' && _typeof(args[2]) === 'object' && args[2] !== null;
}
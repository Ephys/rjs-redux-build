import _classCallCheck from "@babel/runtime/helpers/esm/classCallCheck";
import _createClass from "@babel/runtime/helpers/esm/createClass";
var _jsxFileName = "/Users/ephys/Documents/dev/reworkjs/redux/src/hooks/ClientSideHook.js";
import React from 'react';
import { Provider } from 'react-redux';
import createConfiguredStore from '../create-store';
export var allStores = new Set();

var ClientSideHook =
/*#__PURE__*/
function () {
  function ClientSideHook() {
    _classCallCheck(this, ClientSideHook);

    this.store = createConfiguredStore(); // only used by client app

    allStores.add(this.store);
  }

  _createClass(ClientSideHook, [{
    key: "wrapRootComponent",
    value: function wrapRootComponent(component) {
      return React.createElement(Provider, {
        store: this.store,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 21
        },
        __self: this
      }, component);
    }
  }, {
    key: "postRequest",
    value: function postRequest() {
      allStores["delete"](this.store);
    }
  }]);

  return ClientSideHook;
}();

export { ClientSideHook as default };
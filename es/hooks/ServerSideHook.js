import _classCallCheck from "@babel/runtime/helpers/esm/classCallCheck";
import _createClass from "@babel/runtime/helpers/esm/createClass";
import _possibleConstructorReturn from "@babel/runtime/helpers/esm/possibleConstructorReturn";
import _getPrototypeOf from "@babel/runtime/helpers/esm/getPrototypeOf";
import _inherits from "@babel/runtime/helpers/esm/inherits";
import ThreadLocalValue from '../ThreadLocalValue';
import ClientSideHook from './ClientSideHook';
export var currentStoreTl = new ThreadLocalValue();

var ServerSideHook =
/*#__PURE__*/
function (_ClientSideHook) {
  _inherits(ServerSideHook, _ClientSideHook);

  function ServerSideHook() {
    var _this;

    _classCallCheck(this, ServerSideHook);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ServerSideHook).call(this));
    currentStoreTl.set(_this.store);
    return _this;
  }

  _createClass(ServerSideHook, [{
    key: "preRender",
    value: function preRender(generatedHtmlParts) {
      generatedHtmlParts.footer += "<script>window.__REDUX_SSR__ = ".concat(JSON.stringify(this.store.getState()), "</script>");
    }
  }]);

  return ServerSideHook;
}(ClientSideHook);

export { ServerSideHook as default };
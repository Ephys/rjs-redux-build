import _classCallCheck from "@babel/runtime/helpers/esm/classCallCheck";
import _createClass from "@babel/runtime/helpers/esm/createClass";
import _possibleConstructorReturn from "@babel/runtime/helpers/esm/possibleConstructorReturn";
import _getPrototypeOf from "@babel/runtime/helpers/esm/getPrototypeOf";
import _inherits from "@babel/runtime/helpers/esm/inherits";
import _toConsumableArray from "@babel/runtime/helpers/esm/toConsumableArray";
var _jsxFileName = "/Users/ephys/Documents/dev/reworkjs/redux/src/withRedux.js";
import React from 'react';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { registerStorePart } from './public-api';

function checkInvalidKeys(conf) {
  var authorizedKeys = ['state', 'dispatchers', 'actions', 'connectOptions', 'stores'];
  var invalidKeys = Object.keys(conf).filter(function (key) {
    return !authorizedKeys.includes(key);
  });

  if (invalidKeys.length > 0) {
    throw new TypeError("@withRedux(): configuration contains invalid entries \"".concat(invalidKeys.join('", "'), "\". Only keys allowed are \"").concat(authorizedKeys.join('", "'), "\""));
  }
}

function objNoop() {
  return {};
}
/**
 * Configure a container.
 *
 * @param {!Object} config.stores A list of store parts used by this component.
 * @param {!Object} config.state An mapping of prop names => redux state selector.
 * @param {!Object} config.dispatchers An mapping of prop names => function that will receive dispatch and arguments.
 * @param {!Object} config.actions An mapping of prop names => redux action.
 *
 * @example
 * \@container({
 *   state: {
 *     loggedIn: function() { // this must be a function that returns a selector.
 *       return function selectLoggedInState(state) {
 *         return state.get('loggedIn');
 *       }
 *     },
 *   },
 * })
 * class SomeContainer {}
 *
 * @example
 * \@withRedux({
 *   state: {
 *     loggedIn: SecurityProvider.loggedIn,
 *   },
 *   actions: {
 *     onLogin: function(username, password) {
 *       return { type: 'LOGIN', payload: { username, password } }; // return an action!
 *     },
 *   },
 * })
 * class SomeContainer {}
 *
 * @example
 * \@withRedux({
 *   dispatchers: {
 *     onLogin: function(dispatch, username, password) {
 *       return dispatch({ type: 'LOGIN', payload: { username, password } }); // dispatch an action!
 *     },
 *   },
 * })
 * class SomeContainer {}
 */


export default function withRedux() {
  var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  checkInvalidKeys(config);
  var mapStateToProps;

  if (!config.state) {
    mapStateToProps = objNoop;
  } else {
    var state = config.state;
    var keys = Object.keys(state);
    var values = keys.map(function (key) {
      var val = state[key];

      if (typeof val !== 'function') {
        throw new TypeError("@withRedux({ state[".concat(JSON.stringify(key), "] }) is not a function."));
      }

      return val;
    });
    mapStateToProps = createSelector.apply(void 0, _toConsumableArray(values).concat([function () {
      var merge = {};

      for (var i = 0; i < keys.length; i++) {
        var _key = keys[i];
        merge[_key] = i < 0 || arguments.length <= i ? undefined : arguments[i];
      }

      return merge;
    }]));
  }

  function mapDispatchToProps(dispatch) {
    var result = {};

    if (config.dispatchers) {
      var _loop = function _loop() {
        var key = _Object$keys[_i];
        var dispatcher = config.dispatchers[key];

        if (typeof dispatcher !== 'function') {
          throw new TypeError("@withRedux({ dispatchers[".concat(JSON.stringify(key), "] }) is not a function."));
        }

        result[key] = function callDispatcher() {
          for (var _len = arguments.length, args = new Array(_len), _key2 = 0; _key2 < _len; _key2++) {
            args[_key2] = arguments[_key2];
          }

          dispatcher.apply(void 0, [dispatch].concat(args));
        };
      };

      for (var _i = 0, _Object$keys = Object.keys(config.dispatchers); _i < _Object$keys.length; _i++) {
        _loop();
      }
    }

    if (config.actions) {
      var _loop2 = function _loop2() {
        var key = _Object$keys2[_i2];
        var actionBuilder = config.actions[key];

        if (typeof actionBuilder !== 'function') {
          throw new TypeError("@withRedux({ actions[".concat(JSON.stringify(key), "] }) is not a function."));
        }

        result[key] = function dispatchAction() {
          dispatch(actionBuilder.apply(void 0, arguments));
        };
      };

      for (var _i2 = 0, _Object$keys2 = Object.keys(config.actions); _i2 < _Object$keys2.length; _i2++) {
        _loop2();
      }
    }

    return result;
  }

  var connector = connect(mapStateToProps, mapDispatchToProps, null, config.connectOptions);
  return function setupContainer(wrappedComponent) {
    var wrapperComponent = connector(wrappedComponent);

    if (Array.isArray(config.stores) && config.stores.length > 0) {
      wrapperComponent = withStores(wrapperComponent, config.stores);
    }

    return wrapperComponent;
  };
}

function withStores(WrappedComponent, stores) {
  return (
    /*#__PURE__*/
    function (_React$Component) {
      _inherits(WithStoreParts, _React$Component);

      function WithStoreParts(props) {
        var _this;

        _classCallCheck(this, WithStoreParts);

        _this = _possibleConstructorReturn(this, _getPrototypeOf(WithStoreParts).call(this, props));
        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = stores[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var store = _step.value;
            registerStorePart(store);
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator["return"] != null) {
              _iterator["return"]();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }

        return _this;
      }

      _createClass(WithStoreParts, [{
        key: "render",
        value: function render() {
          return React.createElement(WrappedComponent, Object.assign({}, this.props, {
            __source: {
              fileName: _jsxFileName,
              lineNumber: 165
            },
            __self: this
          }));
        }
      }]);

      return WithStoreParts;
    }(React.Component)
  );
}